defmodule SlowCarbFoodIndex.Repo.Migrations.CreateFoods do
  use Ecto.Migration

  def change do
    create table(:foods) do
      add :name, :string, null: false
      add :type, :string, null: false
      add :description, :text

      timestamps()
    end

    create index(:foods, :name, unique: true)
  end
end
