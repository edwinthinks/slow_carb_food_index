# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     SlowCarbFoodIndex.Repo.insert!(%SlowCarbFoodIndex.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias SlowCarbFoodIndex.Repo
alias SlowCarbFoodIndex.SlowCarbFoods.Food

unlimited_protein_list = [
  "Anchovies",
  "Bass",
  "Beef",
  "Bone Marrow",
  "Chicken",
  "Chicken Livers",
  "Cod",
  "Eel",
  "Eggs",
  "Goat",
  "Haddock",
  "Halibut",
  "Hearts",
  "Herring",
  "Kidney",
  "Lamb",
  "Liver",
  "Mackerel",
  "Mahi Mahi",
  "Ox Liver",
  "Perch",
  "Pork",
  "Red Snapper",
  "Rockfish",
  "Salmon",
  "Sardines",
  "Shellfish",
  "Sweetbreads",
  "Tilapia",
  "Tongue",
  "Tuna",
  "Turkey"
]

unlimited_vegetables_list = [
  "Artichoke Hearts",
  "Arugula",
  "Asparagus",
  "Avocado",
  "Bamboo Shoots",
  "Beet Greens",
  "Bell Peppers",
  "Bok Choy",
  "Broccoli",
  "Broccoli Rabe",
  "Brussels Sprouts",
  "Cabbage",
  "Cauliflower",
  "Celery",
  "Chard",
  "Collards",
  "Cucumbers",
  "Daikon",
  "Dandelion greens",
  "Eggplant",
  "Endive",
  "Fennel Root",
  "Garlic",
  "Green Beans",
  "Iceberg Lettuce",
  "Kale",
  "Kimchi",
  "Kohlrabi",
  "Leeks",
  "Mixed Vegetables",
  "Mushrooms",
  "Mung bean sprouts",
  "Mustard Greens",
  "Napa cabbage",
  "Nori",
  "Olives",
  "Onions",
  "Peas",
  "Peppers (all kinds)",
  "Purslane",
  "Radish",
  "Red cabbage",
  "Romaine Lettuce",
  "Saurkraut",
  "Seaweed (nori)",
  "Spinach",
  "Summer Squash",
  "Swiss Chard",
  "Tomatoes",
  "Turnip Greens",
  "Watercress",
  "Yellow pepper",
  "Zucchini"
]

unlimited_legumes_list = [
  "Black beans",
  "Black-eyed Peas",
  "Cannelloni Beans",
  "Great Northern Beans",
  "Kidney beans",
  "Lentils",
  "Navy Beans",
  "Pinto beans",
  "Red Beans",
]

unlimited_spices_list = [
  "Allspice",
  "Anise",
  "Basil",
  "Bay Leaf",
  "Black Pepper",
  "Cardamom",
  "Cayenne Pepper",
  "Celery seed",
  "Chili Pepper",
  "Chili powder",
  "Cillantro",
  "Cinnamon",
  "Cloves",
  "Coriander Seeds",
  "Cumin",
  "Curry-Green",
  "Curry-Red",
  "Curry-Yellow",
  "Dill",
  "Fennel",
  "Fenugreek",
  "Garam Masala",
  "Garlic",
  "Ginger",
  "Herbs de Provance",
  "Mint",
  "Mustard Seeds",
  "Nutmeg",
  "Oregano",
  "Paprika",
  "Peppermint",
  "Rosemary",
  "Sage",
  "Salt",
  "Tarragon",
  "Thyme",
  "Turmeric"
]

unlimited_fat_oils_nuts_list = [
  "Avocado",
  "Clarified butter",
  "Coconut Oil",
  "Ghee",
  "Macadamia Nut Oil",
  "Olive Oil"
]

unlimited_misc = [
  "Beef Broth",
  "Nut Butters",
  "Chicken Broth",
  "Coconut Milk",
  "Fish Sauce",
  "Hot Sauce",
  "Hummus",
  "Lemon Juice",
  "Lime Juice",
  "Mustard",
  "Pesto",
  "Salsa",
  "Soy Sauce",
  "Sriracha",
  "Tamari (wheat free)",
  "Vinegar"
]

unlimited_foods_list = unlimited_protein_list 
  ++ unlimited_vegetables_list
  ++ unlimited_legumes_list 
  ++ unlimited_spices_list 
  ++ unlimited_fat_oils_nuts_list 
  ++ unlimited_misc

Enum.each(unlimited_foods_list, fn food_name -> 
  Food.changeset(%Food{}, %{name: food_name, type: Food.type_unlimited()})
    |> Repo.insert_or_update
end)

moderation_vegetables_list = [
  "Acorn Squash",
  "Beets",
  "Butternut Squash",
  "Carrots",
  "Jicama",
  "Parsnips",
  "Pumpkin",
  "Squash",
  "Sweet Potato",
  "Yam"
]

moderation_legumes_list = [
  "Chickpeas"
]

moderation_fat_oils_nuts_list = [
  "Almonds",
  "Brazil Nuts",
  "Cashews",
  "Chia seeds",
  "Hazelnuts",
  "Hempseeds",
  "Macadamias",
  "Pecans",
  "Pine Nuts",
  "Pistachios",
  "Pumpkin seeds",
  "Sesame Seeds",
  "Sunflower Seeds",
  "Walnut"
]

moderation_foods_list = moderation_vegetables_list
  ++ moderation_legumes_list
  ++ moderation_legumes_list
  ++ moderation_fat_oils_nuts_list

Enum.each(moderation_foods_list, fn food_name -> 
  Food.changeset(%Food{}, %{name: food_name, type: Food.type_moderation()})
    |> Repo.insert_or_update
end)

restricted_food_list = [
  "Bread (of any kind)",
  "Potatoes",
  "Corn",
  "Quinoa",
  "Pasta",
  "Cereal",
  "Pasta",
  "Tortillas",
  "Grains",
  "Rice (even brown rice)",
  "Sugar",
  "Milk",
  "Cream",
  "Cheese",
  "Yogurt",
  "Apples",
  "Oranges",
  "Grapes",
  "Pears",
  "Strawberries",
]

Enum.each(restricted_food_list, fn food_name -> 
  Food.changeset(%Food{}, %{name: food_name, type: Food.type_restricted()})
    |> Repo.insert_or_update!
end)
