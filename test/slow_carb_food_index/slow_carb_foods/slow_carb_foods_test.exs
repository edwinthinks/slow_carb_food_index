defmodule SlowCarbFoodIndex.SlowCarbFoodsTest do
  use SlowCarbFoodIndex.DataCase

  alias SlowCarbFoodIndex.SlowCarbFoods

  describe "foods" do
    alias SlowCarbFoodIndex.SlowCarbFoods.Food

    @valid_attrs %{description: "some description", name: "some name", type: "unlimited"}
    @update_attrs %{description: "some updated description", name: "some updated name", type: "moderation"} 
    @invalid_attrs %{name: nil, type: nil}
    @invalid_attrs_2 %{name: "some name 2", type: "not-supported"}

    def food_fixture(attrs \\ %{}) do
      {:ok, food} =
        attrs
        |> Enum.into(@valid_attrs)
        |> SlowCarbFoods.create_food()

      food
    end

    test "list_foods/0 returns all foods" do
      food = food_fixture()
      assert SlowCarbFoods.list_foods() == [food]
    end

    test "get_food!/1 returns the food with given id" do
      food = food_fixture()
      assert SlowCarbFoods.get_food!(food.id) == food
    end

    test "create_food/1 with valid data creates a food" do
      assert {:ok, %Food{} = food} = SlowCarbFoods.create_food(@valid_attrs)
      assert food.name == @valid_attrs.name
      assert food.type == @valid_attrs.type
    end

    test "create_food/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = SlowCarbFoods.create_food(@invalid_attrs)
      assert {:error, %Ecto.Changeset{}} = SlowCarbFoods.create_food(@invalid_attrs_2)
    end

    test "create_food/1 with duplicate name returns error changeset" do
      assert {:ok, %Food{} = food} = SlowCarbFoods.create_food(@valid_attrs)
      assert {:error, %Ecto.Changeset{}} = SlowCarbFoods.create_food(@invalid_attrs_2)
    end

    test "update_food/2 with valid data updates the food" do
      food = food_fixture()
      assert {:ok, %Food{} = food} = SlowCarbFoods.update_food(food, @update_attrs)
      assert food.name == @update_attrs.name
      assert food.type == @update_attrs.type
    end

    test "update_food/2 with invalid data returns error changeset" do
      food = food_fixture()
      assert {:error, %Ecto.Changeset{}} = SlowCarbFoods.update_food(food, @invalid_attrs)
      assert {:error, %Ecto.Changeset{}} = SlowCarbFoods.update_food(food, @invalid_attrs_2)
      assert food == SlowCarbFoods.get_food!(food.id)
    end

    test "delete_food/1 deletes the food" do
      food = food_fixture()
      assert {:ok, %Food{}} = SlowCarbFoods.delete_food(food)
      assert_raise Ecto.NoResultsError, fn -> SlowCarbFoods.get_food!(food.id) end
    end

    test "change_food/1 returns a food changeset" do
      food = food_fixture()
      assert %Ecto.Changeset{} = SlowCarbFoods.change_food(food)
    end

  end
end
