defmodule SlowCarbFoodIndexWeb.PageControllerTest do
  use SlowCarbFoodIndexWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "main-app"
  end
end
