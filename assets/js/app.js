// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css";
import FontAwesome from 'react-fontawesome';

import "phoenix_html"

import React from "react"
import ReactDOM from "react-dom"
import {Typeahead} from "react-typeahead"

class MainApp extends React.Component {

  constructor() {
    super();
    this.state = { foods: [], selectedFood: null };

    this.iconProps = this.iconProps.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.getAdvisoryLabel = this.getAdvisoryLabel.bind(this);
    this.scrollIntoView = this.scrollIntoView.bind(this);
  }

  componentDidMount() {
    fetch('/api/foods').then((response) =>{
      return response.json();
    }).then((resJson) => {
      this.setState({foods: resJson.data.foods});
    });
  }

  iconProps() {
    if (!this.state.selectedFood) {
      return { name: 'search', style: {color:"#292b2c"}};
    }

    let type = this.state.selectedFood.type;
    if (type === 'unlimited') {
      return { name: 'check-circle', style: {color:"#5cb85c"} };
    } else if (type === 'moderation') {
      return { name: 'exclamation-circle', style: {color:"#f0ad4e"} };
    } else if (type === 'restricted') {
      return { name: 'times-circle', style: {color:"#d9534f"} };
    }
  }

  getAdvisoryLabel() {
    let type = this.state.selectedFood.type;

    if (type === 'unlimited') {
      return "is allowed in any amount!";
    } else if (type === 'moderation') {
      return "is allowed in restricted amounts";
    } else if (type === 'restricted') {
      return "is NOT allowed! (expect on your cheat day)";
    }
  }

  handleKeyPress(event) {
    if (this.state.selectedFood) {
      this.setState({selectedFood: null})
    }
  }

  scrollIntoView() {
    let typeaheadElement = document.getElementsByClassName('typeahead')[0]

    typeaheadElement.scrollIntoView({
      behavior: 'auto',
      block: 'center',
      inline: 'center'
    });
  }

  render() {
    return (
      <div>
        <div className='icon'>
          <FontAwesome {...this.iconProps()}/>
        </div>

        <div className='selected-food-display'>
          <h1> {this.state.selectedFood ? this.state.selectedFood.name : ""} </h1>

          {
            this.state.selectedFood &&
            <label>{this.getAdvisoryLabel()}</label>
          }
        </div>

        <div className='search-field'>
          <Typeahead
            options={this.state.foods}
            filterOption='name'
            displayOption={(option) => { return option.name; }}
            onOptionSelected={(option) => { this.setState({selectedFood: option}); }}
            onKeyDown={this.handleKeyPress}
            onFocus={this.scrollIntoView}
            placeholder="Enter Food Name"
            maxVisible={6}
            selectFirst
          />
        </div>
      </div>
    )
  }

}

ReactDOM.render(
  <MainApp/>,
  document.getElementById("main-app")
)
