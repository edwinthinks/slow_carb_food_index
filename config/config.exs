# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :slow_carb_food_index,
  ecto_repos: [SlowCarbFoodIndex.Repo]

# Configures the endpoint
config :slow_carb_food_index, SlowCarbFoodIndexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Y21LVMpLUXQM9GtTyY3/3Z9RrRztetnjwHEM260MqqmrzGX/J3eWkHNu/TH40M2G",
  render_errors: [view: SlowCarbFoodIndexWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SlowCarbFoodIndex.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
