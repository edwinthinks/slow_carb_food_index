use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :slow_carb_food_index, SlowCarbFoodIndexWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

default_options = [
  database: System.get_env("POSTGRES_DB") || "slow_carb_food_index_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
]

options = 
  if System.get_env("POSTGRES_USER") do
    default_options ++ [
      username: System.get_env("POSTGRES_USER"),
      password: System.get_env("POSTGRES_PASSWORD")
    ]
  else
    default_options
  end

# Configure your database
config :slow_carb_food_index, SlowCarbFoodIndex.Repo, options

