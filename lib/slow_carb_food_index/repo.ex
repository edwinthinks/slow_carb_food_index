defmodule SlowCarbFoodIndex.Repo do
  use Ecto.Repo,
    otp_app: :slow_carb_food_index,
    adapter: Ecto.Adapters.Postgres
end
