defmodule SlowCarbFoodIndex.SlowCarbFoods.Food do
  use Ecto.Schema
  import Ecto.Changeset

  def types, do: [type_unlimited(), type_moderation(), type_restricted()]
  def type_unlimited, do: "unlimited"
  def type_moderation, do: "moderation"
  def type_restricted, do: "restricted"


  schema "foods" do
    field :description, :string
    field :name, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(food, attrs) do
    food
    |> cast(attrs, [:name, :type, :description])
    |> validate_required([:name, :type])
    |> validate_inclusion(:type, types())
    |> unique_constraint(:name)
  end
end
