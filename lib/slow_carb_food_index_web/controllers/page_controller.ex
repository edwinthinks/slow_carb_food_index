defmodule SlowCarbFoodIndexWeb.PageController do
  use SlowCarbFoodIndexWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
