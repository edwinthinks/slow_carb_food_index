defmodule SlowCarbFoodIndexWeb.FoodController do
  use SlowCarbFoodIndexWeb, :controller

  alias SlowCarbFoodIndex.SlowCarbFoods
  alias SlowCarbFoodIndex.SlowCarbFoods.Food

  def index(conn, _params) do
    foods = SlowCarbFoods.list_foods()
    render(conn, foods: foods)
  end

  def new(conn, _params) do
    changeset = SlowCarbFoods.change_food(%Food{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"food" => food_params}) do
    case SlowCarbFoods.create_food(food_params) do
      {:ok, food} ->
        conn
        |> put_flash(:info, "Food created successfully.")
        |> redirect(to: Routes.food_path(conn, :show, food))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    food = SlowCarbFoods.get_food!(id)
    render(conn, "show.html", food: food)
  end

  def edit(conn, %{"id" => id}) do
    food = SlowCarbFoods.get_food!(id)
    changeset = SlowCarbFoods.change_food(food)
    render(conn, "edit.html", food: food, changeset: changeset)
  end

  def update(conn, %{"id" => id, "food" => food_params}) do
    food = SlowCarbFoods.get_food!(id)

    case SlowCarbFoods.update_food(food, food_params) do
      {:ok, food} ->
        conn
        |> put_flash(:info, "Food updated successfully.")
        |> redirect(to: Routes.food_path(conn, :show, food))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", food: food, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    food = SlowCarbFoods.get_food!(id)
    {:ok, _food} = SlowCarbFoods.delete_food(food)

    conn
    |> put_flash(:info, "Food deleted successfully.")
    |> redirect(to: Routes.food_path(conn, :index))
  end
end
