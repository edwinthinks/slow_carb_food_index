defmodule SlowCarbFoodIndexWeb.FoodView do
  use SlowCarbFoodIndexWeb, :view

  def render("index.json", %{foods: foods}) do
    food_data = Enum.map(foods, fn f -> 
      %{name: f.name, type: f.type, description: f.description} 
    end)

    %{data: %{foods: food_data}}
  end

end
